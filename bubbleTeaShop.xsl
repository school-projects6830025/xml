<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:o="urn:4iz238:dao:bubbleTeaShop"
    exclude-result-prefixes="xs o" version="2.0">

    <xsl:output method="html" encoding="UTF-8" version="5" name="html5" indent="yes"/>

    <xsl:param name="bool">
        <category color="red" name="false"/>
        <category color="green" name="true"/>
    </xsl:param>

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title>Official BubbleTea site with data for employees.</title>
                <link rel="stylesheet" type="text/css" href="style.css"/>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="o:bubbleTeaShop">
        <h1> Bubble Tea menu. </h1>

        <h2 id="toc">Menu</h2>
        <ul>
            <li>
                <a href="#options">options</a>
                <ul>
                    <xsl:for-each select="o:options/child::*">
                        <li>
                            <a href="#{generate-id()}">
                                <xsl:value-of select="name(.)"/>
                            </a>
                        </li>
                    </xsl:for-each>
                </ul>
            </li>
            <li>
                <a href="#recipes">recipes</a>
                <ul>
                    <li>
                        <a href="#main">
                            <xsl:value-of select="o:recipes/o:drink[@type = 'main'][1]/string(@type)"/>
                        </a>
                        <ul>
                            <xsl:for-each select="o:recipes/o:drink[@type = 'main']">
                                <li>
                                    <a href="{generate-id()}.html">
                                        <xsl:value-of select="./o:name"/>
                                    </a>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </li>
                    <li>
                        <a href="#seasonal">
                            <xsl:value-of
                                select="o:recipes/o:drink[@type = 'seasonal'][1]/string(@type)"/>
                        </a>
                        <ul>
                            <xsl:for-each select="o:recipes/o:drink[@type = 'seasonal']">
                                <li>
                                    <a href="{generate-id()}.html">
                                        <xsl:value-of select="o:name"/>
                                    </a>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <xsl:apply-templates select="o:options"/>
        <xsl:apply-templates select="o:recipes"/>
    </xsl:template>

    <xsl:template match="o:recipes">
        <xsl:apply-templates select="o:drink[@type = 'main']"/>
        <xsl:apply-templates select="o:drink[@type = 'seasonal']"/>
    </xsl:template>

    <xsl:template match="o:drink[@type = 'main']">
        <xsl:result-document href="{generate-id()}.html">
            <html lang="en">
                <head>
                    <link rel="stylesheet" type="text/css" href="style.css"/>
                    <title>
                        <xsl:value-of select="name(.)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="string(@type)"/>
                    </title>
                </head>
                <body>
                    <h1>
                        <xsl:value-of select="name(.)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="string(@type)"/>
                    </h1>
                    <ul>
                        <xsl:apply-templates/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="o:name">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>
    
    <xsl:template match="o:image">
        <xsl:variable name="src" select="."/>
        <img src="url({$src})" alt="Picture of a drink from our Menu" width="400px"/>
    </xsl:template>
    
    <xsl:template match="o:drink[@type = 'seasonal']">
        <xsl:result-document href="{generate-id()}.html">
            <html lang="en">
                <head>
                    <link rel="stylesheet" type="text/css" href="style.css"/>
                    <title>
                        <xsl:value-of select="name(.)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="string(@type)"/>
                    </title>
                </head>
                <body>
                    <h1>
                        <xsl:value-of select="name(.)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="string(@type)"/>
                    </h1>
                    <ul>
                        <xsl:apply-templates/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="o:temperatures">
        <h4> Temperatures </h4>
        <xsl:apply-templates select="child::*" mode="childValue"/>
        <a href="#toc">back to menu</a>
    </xsl:template>

    <xsl:template match="o:teaType">
        <h4> Tea type </h4>
        <li>
            <xsl:apply-templates/>
        </li>
        <a href="#toc">back to menu</a>
    </xsl:template>

    <xsl:template match="o:prices">
        <h4> Prices </h4>
        <xsl:apply-templates select="o:price"/>
        <a href="#toc">back to menu</a>
    </xsl:template>

    <xsl:template match="o:price">
        <table border="3" width="200">
            <tr>
                <th width="50%">Size</th>
                <th width="50%">Prices(CZK)</th>
            </tr>
            <xsl:for-each select=".">
                <tr>
                    <td>
                        <xsl:value-of select="string(@size)"/>
                    </td>
                    <td>
                        <xsl:apply-templates/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template match="o:ingredients">
        <h4> Ingredients </h4>

        <table border="3" width="200">
            <tr>
                <th width="75%">Tea</th>
                <th width="25%">Percent</th>
            </tr>
            <tr>
                <td>
                    <xsl:value-of select="o:tea"/>
                </td>
                <td>
                    <xsl:value-of select="o:tea/string(@percent)"/>
                </td>
            </tr>
        </table>
        <table border="3" width="300">
            <tr>
                <th width="50%">Name</th>
                <th width="26%">Type</th>
                <th width="12%">Amount</th>
                <th width="12%">Unit</th>
            </tr>
            <xsl:for-each select=".">
                <xsl:apply-templates select="child::*" mode="ingredients"/>
            </xsl:for-each>
        </table>
        <a href="#toc">back to menu</a>
    </xsl:template>

    <xsl:template match="child::*" mode="ingredients">
        <xsl:choose>
            <xsl:when test="name(.) = 'tea'"> </xsl:when>
            <xsl:otherwise>
                <tr>
                    <td>
                        <xsl:apply-templates/>
                    </td>
                    <td>
                        <xsl:value-of select="name()"/>
                    </td>
                    <td>
                        <xsl:value-of select="string(@amount)"/>
                    </td>
                    <td>
                        <xsl:value-of select="string(@unit)"/>
                    </td>
                </tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="o:sugar">
        <h4> Sweetness Amount </h4>
        <xsl:apply-templates select="o:sweetnessAmount"/>
        <a href="#toc">back to menu</a>
    </xsl:template>

    <xsl:template match="o:sweetnessAmount">
        <table border="3" width="100">
            <tr>
                <th width="50%">Percent</th>
                <th width="50%">Amount</th>
            </tr>
            <xsl:for-each select=".">
                <tr>
                    <td>
                        <xsl:value-of select="string(@percent)"/>
                    </td>
                    <td>
                        <xsl:apply-templates/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template match="o:recommendedToppings">
        <h4> Recommended Toppings</h4>
        <table border="3" width="200">
            <tr>
                <th width="50%">Type</th>
                <th width="50%">Flavour</th>
            </tr>
            <xsl:for-each select="child::*">
                <tr>
                    <td>
                        <xsl:value-of select="name(.)"/>
                    </td>
                    <td>
                        <xsl:value-of select="o:flavour"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
        <a href="#toc">back to menu</a>
    </xsl:template>
    
    <xsl:template match="o:vegan">
        <xsl:variable name="curr" select="."/>
        
        <xsl:choose>
            <xsl:when test="$bool/category[@name = $curr]">
                <div style="color:{$bool/category[@name=$curr]/@color}">
                    <h4> Vegan - <xsl:apply-templates/></h4>
                </div>
            </xsl:when>
            
            <xsl:otherwise>
                <div style="color:{$bool/category[@name=$curr]/@color}">
                    <h4> Vegan - <xsl:apply-templates/></h4>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        <a href="#toc">back to menu</a>
    </xsl:template>
    
    <xsl:template match="o:lactose">
        <xsl:variable name="curr" select="."/>
        
        <xsl:choose>
            <xsl:when test="$bool/category[@name = $curr]">
                <div style="color:{$bool/category[@name=$curr]/@color}">
                    <h4> Lactose - <xsl:apply-templates/></h4>
                </div>
            </xsl:when>
            
            <xsl:otherwise>
                <div style="color:{$bool/category[@name=$curr]/@color}">
                    <h4> Lactose - <xsl:apply-templates/></h4>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        <a href="#toc">back to menu</a>
    </xsl:template>
    
    <xsl:template match="o:description">
        <h4> Description </h4>
        <p>
            <xsl:value-of select="."/>
        </p>
        <a href="#toc">back to menu</a>
    </xsl:template>
    
    <xsl:template match="o:notes">
        <h4> Notes </h4>
        <p>
            <xsl:value-of select="."/>
        </p>
        <a href="#toc">back to menu</a>
    </xsl:template>
    
    <xsl:template match="o:options">
        <h2 id="options">Options</h2>
        <xsl:apply-templates select="child::*" mode="options"/>
    </xsl:template>

    <xsl:template match="child::*" mode="options">
        <h3 id="{generate-id()}">
            <xsl:value-of select="name(.)"/>
        </h3>
        <ul>
            <xsl:choose>
                <xsl:when test="name(.) = 'toppings'">
                    <xsl:for-each select="child::*">
                        <li>
                        <xsl:value-of select="name(.)"/>
                            <ul>
                                <xsl:apply-templates select="child::*" mode="childValue"/>
                            </ul>
                        </li>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="child::*" mode="childValue"/>
                </xsl:otherwise>
            </xsl:choose>
        </ul>
        <a href="#toc">back to menu</a>
    </xsl:template>

    <xsl:template match="child::*" mode="childValue">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    
</xsl:stylesheet>
