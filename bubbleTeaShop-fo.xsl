<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:o="urn:4iz238:dao:bubbleTeaShop" exclude-result-prefixes="xs o" version="2.0">

    <xsl:output method="html" encoding="UTF-8" version="5" name="html5" indent="yes"/>

    <xsl:param name="bool">
        <category color="red" name="false"/>
        <category color="green" name="true"/>
    </xsl:param>

    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="master-title" page-width="500pt"
                    page-height="700pt" margin="40pt">
                    <fo:region-body margin-top="80pt"/>
                    <fo:region-after extent="20pt"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="master-body" page-width="500pt"
                    page-height="700pt" margin="40pt">
                    <fo:region-body margin-bottom="30pt" margin-top="30pt"/>
                    <fo:region-before extent="20pt"/>
                    <fo:region-after extent="20pt"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="master-title" font-family="Helvetica">

                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <fo:leader leader-pattern="rule" leader-length="100%" rule-style="solid"
                            rule-thickness="1pt"/>

                    </fo:block>
                    <fo:block text-align="end">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block text-align="center" font-size="40pt">
                        <xsl:text>
                            Official BubbleTea document with data for employees.
                        </xsl:text>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>

            <fo:page-sequence master-reference="master-body" font-family="Helvetica"
                font-size="12pt">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block-container position="absolute">
                        <fo:block font-size="24pt" font-weight="bold" line-height="24pt"> BubbleTea
                            Manual </fo:block>
                        <fo:block>
                            <fo:leader leader-pattern="rule" leader-length="100%" rule-style="solid"
                                rule-thickness="2pt"/>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <fo:leader leader-pattern="rule" leader-length="100%" rule-style="solid"
                            rule-thickness="1pt"/>
                    </fo:block>
                    <fo:block text-align="center">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block text-align="center" font-size="30pt" id="toc" margin-top="30pt">
                        <xsl:text>
                            Table Of Contents
                        </xsl:text>
                        <xsl:apply-templates select="o:bubbleTeaShop" mode="toc"/>
                    </fo:block>
                    <fo:block>
                        <xsl:apply-templates/>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="o:bubbleTeaShop" mode="toc">
        <fo:block font-size="20pt" font-style="italic" text-align-last="justify">
            <xsl:for-each select="child::*">
                <fo:block>
                    <fo:basic-link internal-destination="{generate-id()}">
                        <xsl:value-of select="name(.)"/>
                        <fo:leader leader-pattern="dots" leader-pattern-width="2pt"
                            leader-alignment="reference-area"/>
                        <fo:page-number-citation ref-id="{generate-id()}"/>
                        <xsl:for-each select="child::*">
                            <fo:block start-indent="20pt">
                                <xsl:choose>
                                    <xsl:when test="../name() = 'recipes'">
                                        <fo:basic-link internal-destination="{generate-id(o:name)}">
                                            <xsl:value-of select="o:name"/>
                                            <fo:leader leader-pattern="dots"
                                                leader-pattern-width="2pt"
                                                leader-alignment="reference-area"/>
                                            <fo:page-number-citation ref-id="{generate-id(o:name)}"
                                            />
                                        </fo:basic-link>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <fo:basic-link internal-destination="{generate-id()}">
                                            <xsl:value-of select="name(.)"/>
                                            <fo:leader leader-pattern="dots"
                                                leader-pattern-width="2pt"
                                                leader-alignment="reference-area"/>
                                            <fo:page-number-citation ref-id="{generate-id()}"/>
                                        </fo:basic-link>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </fo:block>
                        </xsl:for-each>
                    </fo:basic-link>
                </fo:block>
            </xsl:for-each>
        </fo:block>

    </xsl:template>

    <xsl:template match="o:bubbleTeaShop">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="o:options">
        <fo:block padding="4pt" text-align="justify" break-before="page" font-family="Helvetica"
            font-size="34pt" id="{generate-id()}"> Options <xsl:apply-templates select="child::*"
                mode="options"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="child::*" mode="options">
        <fo:block keep-together="always">
            <fo:list-block provisional-distance-between-starts="50%"
                provisional-label-separation="4%" font-size="9pt" text-align="justify"
                id="{generate-id()}" keep-together="always">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()" font-size="24pt">
                        <fo:block>
                            <xsl:value-of select="name(.)"/>
                            <fo:basic-link internal-destination="toc" color="#722F37"
                                margin-left="2mm" font-size="12pt"> back to menu </fo:basic-link>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()" language="en"/>
                </fo:list-item>
            </fo:list-block>

            <fo:list-block provisional-distance-between-starts="50%"
                provisional-label-separation="4%" font-size="9pt" text-align="justify"
                id="{generate-id()}" keep-together="always">
                <xsl:choose>
                    <xsl:when test="name(.) = 'toppings'">
                        <xsl:for-each select="child::*">
                            <fo:list-item>
                                <fo:list-item-label end-indent="label-end()">
                                        <fo:block font-size="14pt" start-indent="140pt" margin-bottom="20pt">
                                            <xsl:value-of select="name(.)"/>
                                        </fo:block>
                                </fo:list-item-label>
                                <fo:list-item-body start-indent="body-start()" language="en">
                                    <fo:block>
                                        <xsl:apply-templates select="child::*" mode="value"/>
                                    </fo:block>
                                </fo:list-item-body>
                            </fo:list-item>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:list-item>
                            <fo:list-item-label end-indent="label-end()"/>
                            <fo:list-item-body start-indent="body-start()" language="en">
                                <fo:block>
                                    <xsl:apply-templates select="child::*" mode="value"/>
                                </fo:block>
                            </fo:list-item-body>
                        </fo:list-item>
                    </xsl:otherwise>
                </xsl:choose>

            </fo:list-block>
        </fo:block>
    </xsl:template>

    <xsl:template match="child::*" mode="value">
        <fo:list-block provisional-distance-between-starts="1em" keep-together="always">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                    <fo:block>&#x2022;</fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()" font-size="14pt">
                    <fo:block>
                        <xsl:apply-templates/>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="o:recipes">
        <fo:block padding="4pt" text-align="justify" break-before="page" font-family="Helvetica"
            font-size="34pt" id="{generate-id()}"> Recipes </fo:block>
        <xsl:apply-templates select="o:drink[@type = 'main']"/>
        <xsl:apply-templates select="o:drink[@type = 'seasonal']"/>
    </xsl:template>

    <xsl:template match="o:drink[@type = 'main']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="o:drink[@type = 'seasonal']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="o:name">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            id="{generate-id()}">
            <xsl:value-of select="."/>
            <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="12pt">
                <fo:basic-link internal-destination="toc" color="#722F37"> back to menu
                </fo:basic-link>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:image">
        <xsl:variable name="src" select="."/>
        <fo:block>
            <fo:external-graphic src="url({$src})" alt="Picture of a drink from our Menu" content-width="140pt"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:temperatures">
        <fo:list-block provisional-distance-between-starts="40%" provisional-label-separation="4%"
            font-size="9pt" text-align="justify">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()" font-size="24pt">
                    <fo:block>
                        <xsl:value-of select="name(.)"/>
                    </fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()" language="en"> </fo:list-item-body>

            </fo:list-item>
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()"> </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()" language="en">
                    <fo:block>
                        <xsl:apply-templates select="child::*" mode="value"/>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="o:teaType">
        <fo:list-block provisional-distance-between-starts="30%" provisional-label-separation="4%"
            font-size="9pt" text-align="justify">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()" font-size="24pt">
                    <fo:block>
                        <xsl:value-of select="name(.)"/>
                    </fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()" language="en"> </fo:list-item-body>

            </fo:list-item>
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()"> </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()" language="en">
                    <fo:block>
                        <fo:list-block provisional-distance-between-starts="1em">
                            <fo:list-item>
                                <fo:list-item-label end-indent="label-end()">
                                    <fo:block>&#x2022;</fo:block>
                                </fo:list-item-label>
                                <fo:list-item-body start-indent="body-start()" font-size="14pt">
                                    <fo:block>
                                        <xsl:apply-templates/>
                                    </fo:block>
                                </fo:list-item-body>
                            </fo:list-item>
                        </fo:list-block>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="o:prices">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            keep-together="always">
            <xsl:value-of select="name(.)"/>
            <fo:block font-size="16pt">
                <fo:table width="110pt" border-style="ridge" border-width="3pt">
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell width="50%" border-style="solid"
                                border-bottom-width="2pt">
                                <fo:block>Size</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="50%" border-style="solid"
                                border-bottom-width="2pt">
                                <fo:block>Price(CZK)</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <xsl:apply-templates/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:price">
        <fo:table-row font-size="12pt">
            <fo:table-cell border-style="solid" border-width="1pt">
                <fo:block>
                    <xsl:value-of select="string(@size)"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell border-style="solid" border-width="1pt">
                <fo:block>
                    <xsl:apply-templates/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="o:ingredients">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            keep-together="always">
            <xsl:value-of select="name(.)"/>
            <fo:table width="110pt" border-style="ridge" border-width="3pt">
                <fo:table-body>
                    <fo:table-row font-size="16pt">
                        <fo:table-cell width="75%" border-style="solid" border-bottom-width="2pt">
                            <fo:block>Tea</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="25%" border-style="solid" border-bottom-width="2pt">
                            <fo:block>Percent</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="12pt">
                        <fo:table-cell border-style="solid" border-width="1pt">
                            <fo:block>
                                <xsl:value-of select="o:tea"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell border-style="solid" border-width="1pt">
                            <fo:block>
                                <xsl:value-of select="o:tea/string(@percent)"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>

            <fo:table width="110pt" border-style="ridge" border-width="3pt" margin-top="4pt">
                <fo:table-body>
                    <fo:table-row font-size="16pt">
                        <fo:table-cell width="50%" border-style="solid" border-bottom-width="2pt">
                            <fo:block>Name</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="26%" border-style="solid" border-bottom-width="2pt">
                            <fo:block>Type</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="12%" border-style="solid" border-bottom-width="2pt">
                            <fo:block>Amount</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="12%" border-style="solid" border-bottom-width="2pt">
                            <fo:block>Unit</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <xsl:for-each select=".">
                        <xsl:apply-templates select="child::*" mode="ingredients"/>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>

        </fo:block>
    </xsl:template>

    <xsl:template match="child::*" mode="ingredients">
        <xsl:choose>
            <xsl:when test="name(.) = 'tea'"> </xsl:when>
            <xsl:otherwise>
                <fo:table-row font-size="12pt">
                    <fo:table-cell border-style="solid" border-width="1pt">
                        <fo:block>
                            <xsl:apply-templates/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="1pt">
                        <fo:block>
                            <xsl:value-of select="name()"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="1pt">
                        <fo:block>
                            <xsl:value-of select="string(@amount)"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="1pt">
                        <fo:block>
                            <xsl:value-of select="string(@unit)"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="o:sugar">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            keep-together="always">
            <xsl:value-of select="name(.)"/>
            <fo:block>
                <fo:table width="110pt" border-style="ridge" border-width="3pt">
                    <fo:table-body>
                        <fo:table-row font-size="16pt">
                            <fo:table-cell width="50%" border-style="solid"
                                border-bottom-width="2pt">
                                <fo:block>Percent</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="50%" border-style="solid"
                                border-bottom-width="2pt">
                                <fo:block>Amount</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <xsl:apply-templates/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:sweetnessAmount">
        <fo:table-row font-size="12pt">
            <fo:table-cell border-style="solid" border-width="1pt">
                <fo:block>
                    <xsl:value-of select="string(@percent)"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell border-style="solid" border-width="1pt">
                <fo:block>
                    <xsl:apply-templates/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="o:recommendedToppings">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            keep-together="always">
            <xsl:value-of select="name(.)"/>
            <fo:block>
                <fo:table width="130pt" border-style="ridge" border-width="3pt">
                    <fo:table-body>
                        <fo:table-row font-size="16pt">
                            <fo:table-cell width="50%" border-style="solid"
                                border-bottom-width="2pt">
                                <fo:block>Type</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="50%" border-style="solid"
                                border-bottom-width="2pt">
                                <fo:block>Flavour</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <xsl:for-each select="child::*">
                            <fo:table-row font-size="12pt">
                                <fo:table-cell width="50%" border-style="solid"
                                    border-bottom-width="2pt">
                                    <fo:block>
                                        <xsl:value-of select="name(.)"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="50%" border-style="solid"
                                    border-bottom-width="2pt">
                                    <fo:block>
                                        <xsl:value-of select="o:flavour"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </xsl:for-each>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:vegan">
        <xsl:variable name="curr" select="."/>

        <xsl:choose>
            <xsl:when test="$bool/category[@name = $curr]">
                <fo:block color="{$bool/category[@name=$curr]/@color}" padding="4pt"
                    text-align="justify" font-family="Helvetica" font-size="24pt"> Vegan -
                    <xsl:apply-templates/>
                </fo:block>
            </xsl:when>
            <xsl:otherwise>
                <fo:block color="{$bool/category[@name=$curr]/@color}" padding="4pt"
                    text-align="justify" font-family="Helvetica" font-size="24pt"> Vegan -
                    <xsl:apply-templates/>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="o:lactose">
        <xsl:variable name="curr" select="."/>

        <xsl:choose>
            <xsl:when test="$bool/category[@name = $curr]">
                <fo:block color="{$bool/category[@name=$curr]/@color}" padding="4pt"
                    text-align="justify" font-family="Helvetica" font-size="24pt"> Lactose -
                    <xsl:apply-templates/>
                </fo:block>
            </xsl:when>

            <xsl:otherwise>
                <fo:block color="{$bool/category[@name=$curr]/@color}" padding="4pt"
                    text-align="justify" font-family="Helvetica" font-size="24pt"> Lactose -
                    <xsl:apply-templates/>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="o:description">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            keep-together="always">
            <xsl:value-of select="name(.)"/>
            <fo:block font-size="12pt">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:notes">
        <fo:block padding="4pt" text-align="justify" font-family="Helvetica" font-size="24pt"
            keep-together="always">
            <xsl:value-of select="name(.)"/>
            <fo:block font-size="12pt" page-break-after="always">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
