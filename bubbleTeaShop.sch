<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process">
    
    <sch:ns uri="urn:4iz238:dao:bubbleTeaShop" prefix="o"/>
    
    <sch:pattern>
        <sch:title>Drink type check</sch:title>
        <sch:rule context="o:recipes">
            <sch:assert test="o:drink[@type='main']">There must be a drink with type main included.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:title>Price check</sch:title>
        <sch:rule context="o:prices">
            <sch:assert test="o:price[@size='small']">There must be a price for size small included.</sch:assert>
            <sch:assert test="o:price[@size='medium']">There must be a price for size medium included.</sch:assert>
            <sch:assert test="o:price[@size='large']">There must be a price for size large included.</sch:assert>
            <sch:report test="o:price &gt; 200">
                Price cannot be greater than 200.
            </sch:report>
            <sch:report test="o:price &lt; 100">
                Price cannot be less than 100.
            </sch:report>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:title>Ingredients check</sch:title>
        <sch:rule context="o:ingredients">
            <sch:report test="o:tea/number(@percent) &gt; 100">
                Tea percent cannot be greater than 100.
            </sch:report>
            <sch:report test="o:tea/number(@percent)&lt; 10">
                Tea percent cannot be less than 10.
            </sch:report>
            <sch:report test="o:powder/number(@amount) &gt; 5">
                Powder amount cannot be greater than 5.
            </sch:report>
            <sch:report test="o:powder/number(@amount)&lt; 1">
                Powder amount cannot be less than 1.
            </sch:report>
            <sch:report test="o:syrup/number(@amount) &gt; 50">
                Syrup amount cannot be greater than 50.
            </sch:report>
            <sch:report test="o:syrup/number(@amount)&lt; 0">
                Syrup amount cannot be less than 0.
            </sch:report>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:title>Sugar check</sch:title>
        <sch:rule context="o:sugar">
            <sch:assert test="o:sweetnessAmount[@percent='25']">There must be a sweetnessAmount information for 25 percent included.</sch:assert>
            <sch:assert test="o:sweetnessAmount[@percent='50']">There must be a sweetnessAmount information for 50 percent included.</sch:assert>
            <sch:assert test="o:sweetnessAmount[@percent='75']">There must be a sweetnessAmount information for 75 percent included.</sch:assert>
            <sch:assert test="o:sweetnessAmount[@percent='100']">There must be a sweetnessAmount information for 100 percent included.</sch:assert>
            <sch:report test="o:sweetnessAmount &gt; 5">
                SweetnessAmount cannot be greater than 5.
            </sch:report>
            <sch:report test="o:sweetnessAmount &lt; 0">
                SweetnessAmount cannot be less than 0.
            </sch:report>
        </sch:rule>
    </sch:pattern>
    
</sch:schema>